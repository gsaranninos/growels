from django import forms  
from growelsapp.models import UserProfileInfo,comments,newsandevents,gettouchwithus,usertokens,enquiry,categories,tags,blog_tags,blogs
from django.contrib.auth.models import User
from django.core.files.images import get_image_dimensions


class UForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password','email')

class UserProfileInfoForm(forms.ModelForm):
 
    class Meta():
        model = UserProfileInfo
        fields = ["first_name", "last_name"]

class usertokensForm(forms.ModelForm):
    class Meta:  
        model = usertokens 
        fields = "__all__"


class enquiryForm(forms.ModelForm):  
    class Meta:  
        model = enquiry
        fields= ["user_name", "user_email","subject","email_message"]  


class CategoriesForm(forms.ModelForm):
    class Meta:  
        model = categories 
        fields = ["Name","meta_title","meta_keyword"]
        
    def clean_Name(self):
        Name = self.cleaned_data.get('Name')
        Name_qs = categories.objects.filter(Name=Name)
        if Name_qs.exists():
            raise forms.ValidationError("Name already exists")
        return Name



class tagsForm(forms.ModelForm):
    class Meta:  
        model = tags 
        fields = ["Name","meta_title","meta_keyword"]


    def clean_Name(self):
        Name = self.cleaned_data.get('Name')
        Name_qs = tags.objects.filter(Name=Name)
        if Name_qs.exists():
            raise forms.ValidationError("Name already exists")
        return Name



class blog_tagsForm(forms.ModelForm):
    class Meta:  
        model = blog_tags 
        fields = ["blog","tag"]


class blogsForm(forms.ModelForm):
    class Meta:  
        model = blogs   
        fields=["category","title","description","image_file","image_alt","image_title","meta_title","meta_keyword"]        


class gettouchwithusForm(forms.ModelForm):
    class Meta:  
        model = gettouchwithus   
        fields='__all__'

class newsandeventsForm(forms.ModelForm):
    class Meta:  
        model = newsandevents 
        fields = ["image_file","title"]

    def clean_image_file(self):
        image_file = self.cleaned_data.get('image_file')
        if image_file:
            w, h = get_image_dimensions(image_file)
            if w < 1000:
                raise forms.ValidationError("The image is %i pixel wide. It's supposed to be 1000px" % w)
            if h < 600:
                raise forms.ValidationError("The image is %i pixel high. It's supposed to be 1000px" % h)
        return image_file

class commentsForm(forms.ModelForm):
    class Meta:  
        model = comments 
        fields = '__all__'        

        
