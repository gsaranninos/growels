
jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'hybrid'
     // mapTypeId: 'satellite'
        // mapTypeId: 'roadmap'
      // mapTypeId: 'terrain'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        ['Growel Feeds Pvt Ltd', 16.39601, 81.14270],
        ['Amalapuram, East Godavari ', 16.57209, 82.00085],
        ['Kakinada, East Godavari ', 16.98906, 82.24746],
        ['Malkipuram, East Godavari ', 16.40944, 81.80333],
        ['Eluru, west Godavari', 16.71066, 81.09524],
        ['Bhimavaram, west Godavari', 16.54489, 81.52124],
        ['Kaikaluru, Krishna', 16.55272, 81.21294],
        ['Gundugolanu, Krishna', 16.75485, 81.22256],
        ['Singarayapalem, Krishna', 16.41062, 81.15201],
        ['Gudivada, Krishna', 16.43000, 80.99000],

        ['Nellore', 14.44260, 79.98646],
        ['Tentulia, North 24 Parganas', 22.78563, 88.85750],
        ['Birbhum', 24.35750, 87.84270],
        ['Basantia, Purba Medinipur', 21.79909, 87.80215],
        ['Contai, East Midnapore', 21.78113, 87.74504],
        ['Midnapore, East Midnapore', 22.42526, 87.30767],
        ['Kakdwip, South 24 Parganas', 21.87603, 88.18528],
        ['Haldia, Purba Medinipur', 22.06667, 88.06981],
        ['Lucknow', 26.84651, 80.94668],
        ['Kedarnagar, Ambedkarnagar', 26.54017, 82.54834],

        ['Chidambaram,Cuddalore', 11.39819, 79.69536],
        ['Ponneri,Chennai', 13.27630, 80.25996],
        ['Velankanni,Nagapattinam', 10.68193, 79.84368],
        ['Ramanathapuram', 10.99745, 76.99160],
        ['Chennai', 13.08160, 80.27518],
        ['Orthanadu,Thanjavur', 10.62864, 79.25309],
        ['Madapuram,Sivagangai', 9.82845, 78.26627],
        ['Rameshwaram,Ramanathapuram', 9.28763, 79.31293],
        ['Sirkazhi,Nagapattinam', 11.23909, 79.73612],
        ['Tanjavur', 10.78700, 79.13783],

        ['Nagapattinam', 10.76880, 79.83943],	  
        ['Balasore', 21.49498, 86.94266],
        ['Balia,Balasore', 21.50298, 86.90551],	  
        ['Raigad', 18.73489, 73.09660],
        ['Mumbai', 19.07598, 72.87766],
        ['Badlapur (Thane),Thane', 19.17201, 73.08338],
        ['Pune', 18.52043, 73.85674],
        ['Navegaon Khairy,Gondia', 20.83510, 80.08143],	  
        ['Calicut', 11.25875, 75.78041],
        ['Cherthala,Alappuzha', 9.68364, 76.33654],

        ['Kollam', 8.89321, 76.61414],
        ['Perumbavoor,Ernakulam', 10.13194, 76.48218],
        ['Ernakulam', 9.98164, 76.29988],
        ['Pozhiyoor,Thiruvanthapuram', 8.30237, 77.09386],
        ['Palakkad', 10.78673, 76.65479],
        ['Thrissur', 10.52764, 76.21443],
        ['Cochin,Ernakulam', 9.93123, 76.26730],
        ['Alleppey,Alappuzha', 9.49807, 76.33885],	  
        ['Margao', 15.27361, 73.95806],	  
        ['Navsari', 20.94670, 72.95203],

        ['Veraval,Girsomnath', 25.09607, 85.31312],	  
        ['Patna', 25.61100, 85.14400],
        ['DumariBujurg,Saran', 25.78665, 85.07010],
        ['Silchar,Cachar', 24.82000, 92.80000],
        ['Bangalore', 12.97160, 77.59456],
        ['Kodagu (Coorg)', 12.42442, 75.73819],
        ['Karwar, Uttara Kannada', 14.80000, 74.13000],	
        //Shrimp Feed
        ['Amalapuram, East Godavari', 16.57209, 82.00085],
        ['Kakinada, East Godavari', 16.98906, 82.24746],
        ['Malkipuram, East Godavari', 16.40944, 81.80333],

        ['Eluru, west Godavari', 16.71066, 81.09524],
        ['Bhimavaram, west Godavari', 16.54489, 81.52124],	  
        ['Elurupadu, west Godavari', 16.51874, 81.34682],
        ['Attili, west Godavari', 16.68848, 81.60372],
        ['Ganapavaram, west Godavari', 16.69938, 81.46355],
        ['Palakollu, west Godavari', 16.51746, 81.72534],
        ['Gundugolanu, west Godavari', 16.78333, 81.23333],
        //Krishna all in feeds
        ['Bapatla, Guntur', 15.50572, 80.04992],
        ['Kota, Nellore', 14.44260, 79.98646],
        ['Gudur, Nellore', 14.14632, 79.85039],

        ['Kavali, Nellore', 14.91318, 79.99298],	  
        //westBengal all in feeds
        //tamil nadu
        ['Pattukkottai,Thanjavur', 10.42528, 79.31403],
        ['Mahabalipuram, Chennai', 12.79122, 80.22256],	  
        //Odisha
        ['Bhadrak, Balasore', 21.47765, 86.84155],
        //Gujarat
        ['Surat', 21.17024, 72.83106],
        ['Valsad', 20.59923, 72.93425],
        ['Jamnagar', 22.47070, 70.05773]
      
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>Growel Feeds Pvt Ltd</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Amalapuram, East Godavari </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kakinada, East Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Malkipuram, East Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Eluru, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bhimavaram, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kaikaluru, Krishna</h3>' +'</div>'],
        ['<div class="info_content">' +
         '<h3>Gundugolanu, Krishna</h3>' +'</div>'],
        ['<div class="info_content">' +
         '<h3>Singarayapalem, Krishna</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Gudivada, Krishna</h3>' +'</div>'],

        ['<div class="info_content">' +
        '<h3>Nellore </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Tentulia, North 24 Parganas</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Birbhum</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Basantia, Purba Medinipur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Contai, East Midnapore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Midnapore, East Midnapore</h3>' +'</div>'],
        ['<div class="info_content">' +
         '<h3>Kakdwip, South 24 Parganas</h3>' +'</div>'],
        ['<div class="info_content">' +
         '<h3>Haldia, Purba Medinipur</h3>' +'</div>'],
        ['<div class="info_content">' +
          '<h3>Lucknow</h3>' +'</div>'],
        ['<div class="info_content">' +
          '<h3>Kedarnagar, Ambedkarnagar</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Chidambaram,Cuddalore </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Ponneri,Chennai</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Velankanni,Nagapattinam</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Ramanathapuram</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Chennai</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Orthanadu,Thanjavur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Madapuram,Sivagangai</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Rameshwaram,Ramanathapuram</h3>' +'</div>'],
        ['<div class="info_content">' +
            '<h3>Sirkazhi,Nagapattinam</h3>' +'</div>'],
        ['<div class="info_content">' +
            '<h3>Tanjavur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Nagapattinam </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Balasore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Balia,Balasore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Raigad</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Mumbai</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Badlapur (Thane),Thane</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Pune</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Navegaon Khairy,Gondia</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Calicut</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Cherthala,Alappuzha</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kollam </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Perumbavoor,Ernakulam</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Ernakulam</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Pozhiyoor,Thiruvanthapuram</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Palakkad</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Thrissur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Cochin,Ernakulam</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Alleppey,Alappuzha</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Margao</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Navsari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Veraval,Girsomnath </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Patna</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>DumariBujurg,Saran</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Silchar,Cachar</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bangalore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kodagu (Coorg)</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Karwar, Uttara Kannada</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Amalapuram, East Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kakinada, East Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Malkipuram, East Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Eluru, west Godavari </h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bhimavaram, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Elurupadu, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Attili, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Ganapavaram, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Palakollu, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Gundugolanu, west Godavari</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bapatla, Guntur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kota, Nellore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Gudur, Nellore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Kavali, Nellore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Pattukkottai,Thanjavur</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Mahabalipuram, Chennai</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bhadrak, Balasore</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Surat</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Valsad</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Jamnagar</h3>' +'</div>'],
        ['<div class="info_content">' +
        '<h3>Bapatla, Guntur</h3>' +'</div>']        

    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(7);
        google.maps.event.removeListener(boundsListener);
    });
    
}