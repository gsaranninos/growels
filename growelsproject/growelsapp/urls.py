from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from .import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [

#front end 
    path('index', views.index,name='growel-feeds-index'), 
    path('about', views.about,name='growel-feeds-about-us'),
    path('aqua-lab', views.aqualab,name='growel-feeds-aqua-lab'), 
    path('blogs', views.blog,name='growel-feeds-blog'), 
    path('contact', views.contact,name='growel-feeds-contact'), 
    path('feeds', views.feeds,name='growel-feeds-feeds'),
    path('growel-story', views.growelsstory,name='growel-feeds-growel-story'), 
    path('landing', views.landing,name='growel-feeds-landing'), 
    path('management-team', views.managementteam,name='growel-feeds-management-team'),
    path('media', views.media,name='growel-feeds-media'), 
    path('mediadetail', views.mediadetails,name='growel-feeds-mediadetail'), 
    path('processor', views.processor,name='growel-feeds-processor'), 
    path('product', views.product,name='growel-feeds-product'), 
    path('quality', views.quality,name='growel-feeds-quality'),
    path('r&d', views.randd,name='growel-feeds-r&d'), 
    path('technology', views.technology,name='growel-feeds-technology'), 

#backend
    path('admin/',views.login,name='growelsapp-login'),
    path('register/',views.register,name='growelsapp-register'),
    path('logout', views.logout, name='temp-logout'),
    path('reset-password',views.resetpassword,name='registrations-password_reset_form'),
    path('reset_password_confirm/<tokens>/',views.resetpasswordconfirm,name='reset_password_confirm'),
    path('reset-password/complete/<tokens>', views.resetpasswordcomplete,name='registrations-password_reset_complete'),
    path('reset-password/success', views.resetpasswordsuccess),
    
    path('passwordchange', views.change_password,name='growelsapp-passwordchange'),
    path('dashboardpage', views.dash_board_pages,name='growelsapp-dashboardpage'),
    path('enquirylist', views.enquirylist,name='growelsapp-enquiry'),
    path('enquirydelete/<int:id>', views.enquirydelete),
   
    path('blogcategories', views.blogcategories,name='growelsapp-blogcategories'),
    path('blogcategorieslist', views.bclist,name='growelsapp-blogcategorieslist'),
    path('blogcategoriesedit/<int:id>/', views.blogcategoryedit),  
    path('blogcategoryupdate/<int:id>', views.blogcategoryupdate),
    path('blogcategoriesdelete/<int:id>', views.blogcategorydestroy),
    path('categoriesdeactive/<int:id>', views.categorieslistdeactive),
    path('categoriesactive/<int:id>', views.categorieslistactive),

    path('blog', views.blogimage,name='growelsapp-blog'),
    path('bloglist', views.blogslist,name='growelsapp-bloglist'),
    path('blogsedit/<int:id>/', views.blogsedit),  
    path('blogsupdate/<int:id>', views.blogsupdate),
    path('blogsdelete/<int:id>', views.blogsdestroy),
    path('bloglistdeactive/<int:id>', views.blogdeactive),
    path('bloglistactive/<int:id>', views.blogactive),

    path('tags', views.tag,name='growelsapp-tags'),
    path('tagslist', views.taglist,name='growelsapp-tagslist'),
    path('tagsedit/<int:id>/', views.tagsedit),  
    path('tagsupdate/<int:id>', views.tagsupdate),
    path('tagsdelete/<int:id>', views.tagsdestroy),
    path('taglistdeactive/<int:id>', views.tagdeactive),
    path('taglistactive/<int:id>', views.tagactive),

    path('blogtags', views.blog_tag,name='growelsapp-blog_tags'),

    path('category/<slug:slug>', views.blogcategoryonclick,name='growelsapp-blogcategoryonclick'),
    path('blog/<slug:slug>', views.blogreadmoreonclick,name='growelsapp-blogreadmoreonclick'),
    path('blogtags/<slug:slug>', views.tagsonclick,name='growelsapp-blogreadmoreonclick'),

    
    path('touchlist', views.gettouchwithuslist,name='temp-gettouchwithuslist'),

    path('touchdelete/<int:id>', views.gettouchwithusdelete),

    path('newsandevents',views.news,name='growelsapp-newsandevents'),
    path('newsandeventslist', views.newslist,name='growelsapp-newsandeventslist'),
    path('newsandeventsedit/<int:id>/', views.newsedit,name='growelsapp-newsandeventsedit'),  
    path('newsandeventsupdate/<int:id>', views.newsupdate),
    path('newsandeventsdestory/<int:id>', views.newsdestroy),

    path('newsandeventsdeactive/<int:id>', views.deactivenewsandeventslist),

    path('newsandeventsactive/<int:id>', views.activenewsandeventslist),

    #path('comment', views.comments),


]+static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)

